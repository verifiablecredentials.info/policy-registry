package com.crosswordcybersecurity.utils;

import org.springframework.http.HttpStatus;

public class CommonResponse {
    private Object response;
    private HttpStatus code;
    private String message;
    private boolean result;
    private String exceptionMessage;

    public CommonResponse() {
        response = null;
        code = HttpStatus.NOT_IMPLEMENTED;
        message = "";
        result = false;
        exceptionMessage = "";
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    public HttpStatus getCode() {
        return code;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }
}
