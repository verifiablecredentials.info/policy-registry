package com.crosswordcybersecurity.utils.db;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class Adaptor {

    public static JSONObject toJSON (Object object){
        Gson gson = new Gson();
        String sObject = gson.toJson(object);
        JSONObject jsonObject = new JSONObject(sObject);
        return jsonObject;
    }

    public static JSONArray toJSONArray (List vcSkels) {
        Gson gson = new Gson();
        String sVcSkels = gson.toJson(vcSkels);
        JSONArray jsonArray = new JSONArray(sVcSkels);
        return jsonArray;
    }

}
