package com.crosswordcybersecurity.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("policy")
public class ConfigurationPolicy {
    private String dbPath;
    private String defaultFormat;

    public ConfigurationPolicy() {
    }

    public String getDbPath() { return dbPath; }

    public void setDbPath(String dbPath) { this.dbPath = dbPath; }

    public String getDefaultFormat() {
        return defaultFormat;
    }

    public void setDefaultFormat(String defaultFormat) {
        this.defaultFormat = defaultFormat;
    }

}
