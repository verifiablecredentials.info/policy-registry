package com.crosswordcybersecurity.utils;

import com.couchbase.lite.CouchbaseLiteException;
import com.crosswordcybersecurity.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.crosswordcybersecurity.model.SearchRequestObject;
import com.crosswordcybersecurity.utils.db.Adaptor;
import info.verifiablecredentials.policydb.PolicyDbApi;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class Common {

    @Autowired
    private ConfigurationPolicy config;

    private static final Logger log = LoggerFactory.getLogger(Common.class);

    private final ObjectMapper objectMapper = new ObjectMapper();

    private CommonResponse response;

    public PolicyDbApi initDb() {
        // Initialize Database APIs
        PolicyDbApi policyDbApi = new PolicyDbApi();
        String sDbPath = config.getDbPath();
        checkConfigParameter(sDbPath, "policy.dbPath");
        policyDbApi.initCouchbase(sDbPath);
        return policyDbApi;
    }

    public CommonResponse callSearchDb(Object policyMatch, String policyFormat, PolicyDbApi policyDbApi) {
        response = new CommonResponse();

        // Search DB (Policy Match)
        SearchRequestObject dbSearchRequestObject = new SearchRequestObject();
        dbSearchRequestObject.setPolicyMatch(policyMatch);
        dbSearchRequestObject.setPolicyFormat(policyFormat);
        try {
//            Object policy = searchApi.searchPolicies(dbSearchRequestObject);
            JSONObject jPolicyMatch = Adaptor.toJSON(dbSearchRequestObject);
            JSONObject policy = policyDbApi.getPolicy(jPolicyMatch);

            if (policy.getClass().equals(JSONObject.class)) {
                JSONObject joPolicy = policy;
                if (joPolicy.has("policy") && joPolicy.get("policy") != null) {
                    String sPolicy = policy.get("policy").toString();
                    Map<String, Object> hmPolicy = objectMapper.readValue(sPolicy, Map.class);

                    response.setResult(true);
                    response.setResponse(hmPolicy);

                } else {
                    response.setMessage("Policy Not Found");
                    response.setCode(HttpStatus.NOT_FOUND);
                }
            } else {
                response.setMessage("Policy Not Found");
                response.setCode(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            response.setMessage("Error retriving policy");
            response.setCode(HttpStatus.NOT_FOUND);
            response.setExceptionMessage(e.getMessage());
        }

        if (! response.isSuccess()) {
            log.error(response.getMessage() + " - " + response.getExceptionMessage());
        }
        return response;
    }

    public CommonResponse callExtractSearchParameters(SearchRequestObject body) {
        response = new CommonResponse();

        // Receive TLS Connection Request (HTTPS)
        // Receive Search DB over TLS

        // Extract Parameters
        // Request OK? No - Badly Formed Request - Close TLS Connection
        if (body == null || body.getPolicyMatch() == null) {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage("Badly Formed Request");
        } else {
            response.setResult(true);

            // Set default policy format if none (null or empty) is informed
            if (body.getPolicyFormat() == null || body.getPolicyFormat().equals("")) {
                log.info("Using default policyFormat.");
                body.setPolicyFormat(config.getDefaultFormat());
            }
        }

        if (! response.isSuccess()) {
            log.error(response.getMessage() + " - " + response.getExceptionMessage());
        }
        return response;
    }

    public CommonResponse callExtractInsertVCPolicyParameters(VcPolicyBody body) {

        response = new CommonResponse();

        // Receive TLS Connection Request (HTTPS)
        // Receive Search DB over TLS

        // Extract Parameters
        // Request OK? No - Badly Formed Request - Close TLS Connection
        if (body == null || body.getPolicyMatch() == null || body.getVcpolicy() == null) {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage("Badly Formed Request");
        } else {
            response.setResult(true);

            // Set default policy format if none (null or empty) is informed
            if (body.getPolicyFormat() == null || body.getPolicyFormat().equals("")) {
                log.info("Using default policyFormat.");
                body.setPolicyFormat(config.getDefaultFormat());
            }

        }

        if (! response.isSuccess()) {
            log.error(response.getMessage() + " - " + response.getExceptionMessage());
        }
        return response;

    }

    public CommonResponse callInsertPolicyInDb(VcPolicyBody vcPolicy, PolicyDbApi policyDbApi) {
        response = new CommonResponse();

        try {
            JSONObject joVcPolicy = Adaptor.toJSON(vcPolicy.getVcpolicy());
            String sVcPolicy = joVcPolicy.toString();

            Map<String, Object> hmPolicy = new HashMap<>();
            hmPolicy.put("policy", sVcPolicy);
            hmPolicy.put("policyMatch", vcPolicy.getPolicyMatch());
            hmPolicy.put("policyFormat", vcPolicy.getPolicyFormat());

            JSONObject joPolicy = Adaptor.toJSON(hmPolicy);
            policyDbApi.insertPolicy(joPolicy);

            response.setResult(true);
            response.setCode(HttpStatus.OK);
            response.setMessage("OK");

        } catch (JsonProcessingException e) {
            response.setMessage("Serialisation Error");
            response.setExceptionMessage(e.getMessage());
            response.setCode(HttpStatus.INTERNAL_SERVER_ERROR);

        } catch (CouchbaseLiteException e) {
            response.setMessage("Database Error");
            response.setExceptionMessage(e.getMessage());
            response.setCode(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (! response.isSuccess()) {
            log.error(response.getMessage() + " - " + response.getExceptionMessage());
        }
        return response;
    }

    private void checkConfigParameter(Object parameter, String parameterPath) {
        if (parameter == null) {
            log.warn(parameterPath + " configuration parameter not found.");
        }
    }

    public <T> ResponseEntity<T> exit(String endpoint, String message, HttpStatus code, PolicyDbApi policyDbApi) {
        return exit(endpoint, message, code, policyDbApi, null);
    }

    public <T> ResponseEntity<T> exit(String endpoint, String message, HttpStatus code, PolicyDbApi policyDbApi, T responseObject) {
        if (code != HttpStatus.OK) {
            log.error(message);
        } else if (responseObject != null) {
            log.info("Sent Message:");
            log.info(responseObject.toString());
        }
        log.info("=== " + endpoint + " - End ===");
        if (responseObject == null) {
            return new ResponseEntity<>(code);
        } else {
            return new ResponseEntity<>(responseObject, code);
        }
    }

    public void begin(String endpoint, Object body) {
        log.info("=== " + endpoint + " - Begin ===");

        log.info("Received Message:");
        log.info(body.toString());
    }

}


