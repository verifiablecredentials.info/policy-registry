package com.crosswordcybersecurity.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * SearchRequestObject
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-01-08T18:42:23.360-03:00[America/Recife]")
public class SearchRequestObject {
  @JsonProperty("policyMatch")
  private Object policyMatch = null;

  @JsonProperty("policyFormat")
  private String policyFormat = null;

  public SearchRequestObject policyMatch(Object policyMatch) {
    this.policyMatch = policyMatch;
    return this;
  }

  /**
   * Get policyMatch
   * @return policyMatch
  **/
  @ApiModelProperty(example = "{\"type\":\"Over 18\"}", value = "")
  
    public Object getPolicyMatch() {
    return policyMatch;
  }

  public void setPolicyMatch(Object policyMatch) {
    this.policyMatch = policyMatch;
  }

  public SearchRequestObject policyFormat(String policyFormat) {
    this.policyFormat = policyFormat;
    return this;
  }

  /**
   * Get policyFormat
   * @return policyFormat
   **/
  @ApiModelProperty(example = "Identiproof", value = "")

  public String getPolicyFormat() { return policyFormat; }

  public void setPolicyFormat(String policyFormat) {
    this.policyFormat = policyFormat;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SearchRequestObject searchRequestObject = (SearchRequestObject) o;
    return Objects.equals(this.policyMatch, searchRequestObject.policyMatch) &&
            Objects.equals(this.policyFormat, searchRequestObject.policyFormat);
  }

  @Override
  public int hashCode() {
    return Objects.hash(policyMatch, policyFormat);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SearchRequestObject {\n");
    
    sb.append("    policyMatch: ").append(toIndentedString(policyMatch)).append("\n");
    sb.append("    policyFormat: ").append(toIndentedString(policyFormat)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
