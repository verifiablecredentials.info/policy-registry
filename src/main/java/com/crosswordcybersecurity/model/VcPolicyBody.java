package com.crosswordcybersecurity.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * VcPolicyBody
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-18T15:30:15.088Z[Europe/London]")


public class VcPolicyBody   {
  @JsonProperty("policyMatch")
  private Object policyMatch = null;

  @JsonProperty("policyFormat")
  private String policyFormat = null;

  @JsonProperty("vcpolicy")
  private Object vcpolicy = null;

  public VcPolicyBody policyMatch(SearchRequestObject searchRequestObject) {
    this.policyMatch = searchRequestObject;
    return this;
  }

  /**
   * Get policyMatch
   * @return policyMatch
  **/
  @ApiModelProperty(value = "")
  
    @Valid
    public Object getPolicyMatch() {
    return policyMatch;
  }

  public void setPolicyMatch(Object policyMatch) {
    this.policyMatch = policyMatch;
  }

  public VcPolicyBody vcpolicy(Object vcpolicy) {
    this.vcpolicy = vcpolicy;
    return this;
  }

  public VcPolicyBody policyFormat(String policyFormat) {
    this.policyFormat = policyFormat;
    return this;
  }

  /**
   * Get policyFormat
   * @return policyFormat
   **/
  @ApiModelProperty(example = "Identiproof", value = "")

  public String getPolicyFormat() { return policyFormat; }

  public void setPolicyFormat(String policyFormat) {
    this.policyFormat = policyFormat;
  }

  /**
   * Get vcpolicy
   * @return vcpolicy
  **/
  @ApiModelProperty(value = "")
  
    @Valid
    public Object getVcpolicy() {
    return vcpolicy;
  }

  public void setVcpolicy(Object vcpolicy) {
    this.vcpolicy = vcpolicy;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VcPolicyBody vcPolicyBody = (VcPolicyBody) o;
    return Objects.equals(this.policyMatch, vcPolicyBody.policyMatch) &&
        Objects.equals(this.policyFormat, vcPolicyBody.policyFormat) &&
        Objects.equals(this.vcpolicy, vcPolicyBody.vcpolicy);
  }

  @Override
  public int hashCode() {
    return Objects.hash(policyMatch, policyFormat, vcpolicy);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VcPolicyBody {\n");
    
    sb.append("    policyMatch: ").append(toIndentedString(policyMatch)).append("\n");
    sb.append("    policyFormat: ").append(toIndentedString(policyFormat)).append("\n");
    sb.append("    vcpolicy: ").append(toIndentedString(vcpolicy)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
