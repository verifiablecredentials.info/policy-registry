package com.crosswordcybersecurity.api;

import com.crosswordcybersecurity.model.SearchRequestObject;
import com.crosswordcybersecurity.utils.CommonResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.verifiablecredentials.policydb.PolicyDbApi;
import io.swagger.annotations.*;
import com.crosswordcybersecurity.utils.Common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-01-08T18:42:23.360-03:00[America/Recife]")
@Controller
public class SearchApiController implements SearchApi {

    private static final Logger log = LoggerFactory.getLogger(SearchApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private Common common;

    @org.springframework.beans.factory.annotation.Autowired
    public SearchApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Object> searchPolicies(@ApiParam(value = "A Policy Match JSON object." ,required=true )  @Valid @RequestBody SearchRequestObject body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {

            PolicyDbApi policyDbApi = common.initDb();

            CommonResponse response = new CommonResponse();
            String endpoint = "Search";

            common.begin(endpoint, body);

            try {
                log.debug("EXTRACT PARAMETERS");
                response = common.callExtractSearchParameters(body);
                if (! response.isSuccess()) {
                    return common.exit(endpoint, response.getMessage(), response.getCode(), policyDbApi);
                }

                log.debug("SEARCH DB (POLICY MATCH)");
                response = common.callSearchDb(body.getPolicyMatch(), body.getPolicyFormat(), policyDbApi);
                if (! response.isSuccess()) {
                    return common.exit(endpoint, response.getMessage(), response.getCode(), policyDbApi);
                }
                Object policy = response.getResponse();

                // Return Policy - Close TLS Connection
                return common.exit(endpoint, "Success", HttpStatus.OK, policyDbApi, policy);

            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
    }

}
