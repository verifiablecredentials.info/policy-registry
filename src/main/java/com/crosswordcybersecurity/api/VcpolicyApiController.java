package com.crosswordcybersecurity.api;

import com.crosswordcybersecurity.utils.CommonResponse;
import com.crosswordcybersecurity.model.SearchRequestObject;
import com.crosswordcybersecurity.model.VcPolicyBody;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import com.crosswordcybersecurity.utils.Common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import info.verifiablecredentials.policydb.PolicyDbApi;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-18T15:30:15.088Z[Europe/London]")
@Controller
public class VcpolicyApiController implements VcpolicyApi {

    private static final Logger log = LoggerFactory.getLogger(VcpolicyApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private Common common;

    @org.springframework.beans.factory.annotation.Autowired
    public VcpolicyApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<VcPolicyBody> getVCPolicy(@ApiParam(value = "The body of the request contains the Policy Match JSON object" ,required=true )  @Valid @RequestBody SearchRequestObject body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {

            PolicyDbApi policyDbApi = common.initDb();

            CommonResponse response = new CommonResponse();
            String endpoint = "Get VC Policy";

            common.begin(endpoint, body);

            log.debug("EXTRACT PARAMETERS");
            response = common.callExtractSearchParameters(body);
            if (!response.isSuccess()) {
                return common.exit(endpoint, response.getMessage(), response.getCode(), policyDbApi);
            }

            Object policyMatch = body.getPolicyMatch();
            String policyFormat = body.getPolicyFormat();

            log.debug("SEARCH DB (POLICY MATCH)");
            response = common.callSearchDb(policyMatch, policyFormat, policyDbApi);
            if (!response.isSuccess()) {
                return common.exit(endpoint, response.getMessage(), response.getCode(), policyDbApi);
            }
            Object policy = response.getResponse();

            VcPolicyBody vcPolicyBody = new VcPolicyBody();
            vcPolicyBody.setVcpolicy(policy);
            vcPolicyBody.setPolicyMatch(policyMatch);
            vcPolicyBody.setPolicyFormat(policyFormat);

            // Return Policy - Close TLS Connection
            return common.exit(endpoint, "Success", HttpStatus.OK, policyDbApi, vcPolicyBody);
        }
        return new ResponseEntity<VcPolicyBody>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<String> insertVCPolicy(@ApiParam(value = "The body of the request contains the Policy Match JSON object and the VC Policy JSON object" ,required=true )  @Valid @RequestBody VcPolicyBody body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {

            PolicyDbApi policyDbApi = common.initDb();

            CommonResponse response = new CommonResponse();
            String endpoint = "Insert VC Policy";

            common.begin(endpoint, body);

            log.debug("EXTRACT PARAMETERS");
            response = common.callExtractInsertVCPolicyParameters(body);
            if (!response.isSuccess()) {
                return common.exit(endpoint, response.getMessage(), response.getCode(), policyDbApi);
            }

            log.debug("INSERT VC POLICY IN DB");
            response = common.callInsertPolicyInDb(body, policyDbApi);
            if (!response.isSuccess()) {
                return common.exit(endpoint, response.getMessage(), response.getCode(), policyDbApi);
            }

            return common.exit(endpoint, response.getMessage(), response.getCode(), policyDbApi);

        }
        return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
    }

}
